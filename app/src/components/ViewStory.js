import React, { useState, useEffect } from "react";
import { StyleSheet, Text, FlatList, View, TouchableHighlight, Button, SafeAreaView, Image, ActivityIndicator, Linking } from "react-native";


const ViewStory = () => {
   const [data, setdata] = useState([]);
   const [isLoading, setisLoading] = useState(true);
   const [status, setstatus] = useState(0);

    useEffect(() => {
        getMoviesFromApiAsync();
        return () => {

        }
    }, [status])
    function getMoviesFromApiAsync  ()  {
        fetch('https://6803-14-226-0-194.ap.ngrok.io/get-all-stories')
        .then((response) => response.json())
        .then((responseJson) => {
            setdata(responseJson.items);
            // console.log(responseJson.items);
        })
        .catch((error) => {
        console.error(error);
        }).finally(() => setisLoading(false))
    }

    const renderItem = ({item, index}) => {
         return (
            <View style={{marginTop: 10, flexDirection: 'row', alignContent: 'center', width: '90%'}}>
                <Image source={{uri : item.image}} style={{width: 150, height: 150}} resizeMode='contain'/>
                <View>
                    <Text style={{textAlign: "justify", color: "#000", fontWeight: "700"}}>{item.title}</Text>
                    <Text style={{textAlign: "left", fontWeight: "200"}}>{item.info}</Text>
                    <Text style={{textAlign: "left", color: "blue"}} onPress={() => {Linking.openURL(item.link)}}>Link</Text>
                    <Text style={{textAlign: "left"}}>{item.description}</Text>
                </View>
            </View>
         );
    }
   return(
      <SafeAreaView>
        {isLoading ? <ActivityIndicator/> : (
           <FlatList data={data}
           renderItem={renderItem}
           keyExtractor={item => `key-${item.id}`}/>
        )}
      </SafeAreaView>
   );
};

export default ViewStory;