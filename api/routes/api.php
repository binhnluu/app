<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\StoryController;
use App\Http\Controllers\UserLoginController;
use App\Http\Controllers\ChapterController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('user/login', [UserLoginController::class, 'store']);

Route::group(['prefix' => 'user'], function() {
    //
    Route::post('/', [UserController::class, 'store'])->name('store');
});

// Story
Route::group(['prefix' => 'story', 'as' => 'story'], function() {
    Route::get('/', [StoryController::class, 'index'])->name('index');
    Route::post('/store', [StoryController::class, 'store'])->name('store');
    Route::post('/all', [StoryController::class, 'storeAll'])->name('store-all');
});

// Chapter
Route::group(['prefix' => 'chapter', 'as' => 'chapter'], function() {
    Route::get('/', [ChapterController::class, 'index'])->name('index');
    // Route::post('/store', [ChapterController::class, 'store'])->name('store');
    Route::post('/all', [ChapterController::class, 'ChapterAll'])->name('chapter-all');
});

