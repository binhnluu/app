<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Requests\UserLoginRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{

    public function store(UserLoginRequest $request) {
        // dd($request->all());
        DB::beginTransaction();
        try {
            $user = new User();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->level = $request->level ?? 1;
            $user->status = $request->status ?? false;
            $user->save();
            DB::commit();
            
        } catch (\PDOException $e) {
            DB::rollBack();
            return $this->respond(503, 'NG', ['message' => $e->getMessage()]);
        }
        return $this->respond(200, 'OK', []);
    }
}
