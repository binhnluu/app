<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Weidner\Goutte\GoutteFacade;
use App\Models\Story;
use DB;
use Str;

class StoryController extends Controller
{
    public function index() {
        try {
            $data = Story::all()->toArray();
        }
        catch(\PDOException $e){
            return $this->respond(503, 'NG', ['message' => $e->getMessage()]);
        }
        
        return $this->respond(200, 'OK', $data); 
    }
    
    public function storeAll(Request $request) {
        $url = $request->url;
        if (!empty($url)) {
            $getDataByUrl = GoutteFacade::request('GET', $url);
            $urlStories = $getDataByUrl->filter('h3.truyen-title')->each(function ($urlStory) {
                return $urlStory->filter('a')->attr('href');
            });
            if (!empty($urlStories)) {
                DB::beginTransaction();
                try {
                    foreach ($urlStories as $link) {
                        $urlRequest = GoutteFacade::request('GET', $link);
                        $data = [
                            'name' => $this->crawlerData($urlRequest, 'div.col-truyen-main h3'),
                            'slug' => Str::slug($this->crawlerData($urlRequest, 'div.col-truyen-main h3')),
                            'alias' => $this->crawlerData($urlRequest, 'div.info a[itemprop="author"]'),
                            'content' => $this->crawlerData($urlRequest),
                            'source' => $this->crawlerData($urlRequest, 'div.info span.source'),
                            'image' => $this->crawlerData($urlRequest, 'div.book img', 'src'),
                            'description' => $this->crawlerData($urlRequest, 'div.desc-text')
                        ];
                        Story::create($data);
                    }
                    DB::commit();
                }
                catch(\PDOException $e) {
                    DB::rollBack();
                    return $this->respond(503, 'NG', ['message' => $e->getMessage()]);
                }
                return $this->respond(200, 'OK', []);
            }
        }
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try{
            $url = $request->url;
            $data['name'] = $request->name;
            $data['slug'] = $request->slug;
            $data['alias'] = $request->alias;
            $data['content'] = $request->content;
            $data['source'] = $request->source;
            $data['image'] = $request->image;
            $data['description'] = $request->description;
            if (!empty($url)) {
                $urlRequest = GoutteFacade::request('GET', $url);
                    $data['name'] = $this->crawlerData($urlRequest, 'div.col-truyen-main h3');
                    $data['slug'] = Str::slug($this->crawlerData($urlRequest, 'div.col-truyen-main h3'));
                    $data['alias'] = $this->crawlerData($urlRequest, 'div.info a[itemprop="author"]');
                    $data['content'] = $this->crawlerData($urlRequest);
                    $data['source'] = $this->crawlerData($urlRequest, 'div.info span.source');
                    $data['image'] = $this->crawlerData($urlRequest, 'div.book img', 'src');
                    $data['description'] = $this->crawlerData($urlRequest, 'div.desc-text');
            }
            Story::create($data);
            DB::commit();
        }catch(\PDOException $e) {
            DB::rollBack();
            return $this->respond(503, 'NG', ['message' => $e->getMessage()]);
        }
        return $this->respond(200, 'OK', $data);
    }
   
}
