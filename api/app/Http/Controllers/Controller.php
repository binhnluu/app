<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public static function respond($code = 200, $msg = '', $items = [])
    {
        $json = [
            'code'  => $code,
            'msg'   => $msg,
            'date'  => time(),
            'items' => [],
        ];
        if ($items) {
            foreach ($items as $key => $item) {
                $json['items'][$key] = $item;
                switch ($key) {
                    case 'created_at':
                    case 'updated_at':
                        if (is_object($json['items'][$key])) {
                            $json['items'][$key] = (string)$json['items'][$key];
                        }
                        break;
                }
            }
        }
        return response()->json($json);
    }

    protected function crawlerData($v, $char = null, $type = '')
    {
        if (!empty($char)) {
            if ($type == 'src') {
                return $v->filter($char)->attr('src');
            }
            elseif($type == 'href') {
                return $v->filter($char)->attr('href');
            }
            else {
                return $v->filter($char)->text();
            }
        }

        return '';
    }
}
