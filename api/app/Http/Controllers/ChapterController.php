<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Weidner\Goutte\GoutteFacade;
use App\Models\Story;
use App\Models\Chapter;
use Str;
use DB;

class ChapterController extends Controller
{

    public function index() {
        try {
            $data = Chapter::all()->toArray();
        }
        catch(\PDOException $e){
            return $this->respond(503, 'NG', ['message' => $e->getMessage()]);
        }
        
        return $this->respond(200, 'OK', $data); 
    }
    //
    public function ChapterAll(Request $request){
        DB::beginTransaction();
        try {
            $crawler = GoutteFacade::request('GET', $request->url);
            $slug = Str::slug($this->crawlerData($crawler, 'div#truyen h3.title'));
            $story_id = Story::where('slug', $slug)->pluck('id')->first();

            // insert
            $crawler->filter('ul.list-chapter li')->each(function ($node) use ($story_id) {
                $crawlerSub = GoutteFacade::request('GET', $this->crawlerData($node, 'a', 'href'));
                Chapter::create(
                    [
                        'name' => $this->crawlerData($node, 'a'),
                        'subname' => Str::slug($this->crawlerData($node, 'a')),
                        'alias' => $this->crawlerData($node, 'a', 'href'),
                        'content' => $this->crawlerData($crawlerSub, 'div#chapter-c'),
                        'story_id' => $story_id,
                    ]
                );
            });
            DB::commit();
        }catch(\PDOException $e){
            DB::rollBack();
            return $this->respond(503, 'NG', ['message' => $e->getMessage()]);
        }

        return $this->respond(200, 'OK', []);
    }
}
