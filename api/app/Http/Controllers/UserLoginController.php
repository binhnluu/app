<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

use Illuminate\Http\Request;

class UserLoginController extends Controller
{
    //
    public function store(Request $request) {

        DB::beginTransaction();
        try {
            $users = User::where('email', $request->email)
                    ->first()->toArray();
                    // ->take(1)->get()->toArray();

            $msg = 'email_address_or_password_incorrect';

            if (empty($users)) {
                return $this->respond(422, $msg, []);
            }

            $USER_TYPE_ID = config('common.USER_TYPE_ID');
            $items = [
                'name' => $users['name'],
                'email' => $users['email'],
                'status' => $users['level']

            ];

            if (Hash::check($request->password, $users['password'])) {
                if ($USER_TYPE_ID['SUPER_ADMIN'] != $users['level']) {
                    $msg = 'No access';
                    return $this->respond(403, $msg, $items);
                }elseif($users['status'] == true){
                    $msg = 'Account Lock';
                    return $this->respond(403, $msg, []);
                }
            }else {
                return $this->respond(422, $msg, []);
            }

            DB::commit();
            return $this->respond(200, 'OK', $items);
            
        }catch(\PDOException $e) {
            DB::rollBack();
            return $this->respond(422, $msg, []);
        }

    }
}
