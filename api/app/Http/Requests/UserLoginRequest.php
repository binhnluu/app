<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class UserLoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name'       => 'required',
            'email'    => 'required|email|unique:users,email,',
            // 'email'    => 'required|email|unique:users,email,'.$this->user->id,
            'password' => 'required',
            'level'    => 'integer|nullable|between:1,2',
            'status'    => 'boolean|nullable'
        ];

        return $rules;
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            Controller::respond(422, [
                    'message' => 'failed validation.',
                    'errors'  => $validator->errors()->toArray()
                ]
            )
        );
    }
}
