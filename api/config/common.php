<?php

return [
    'USER_TYPE_ID' => 
    [
        'SUPER_ADMIN' => 99,
        'ADMIN'       => 1,
        'STAFF'       => 2,
        'USER'        => 3,
    ],
];